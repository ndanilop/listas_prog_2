#include <stdio.h>

int inverte(int *vx, int *vy, int n);
int main (){
    int a[10], y[10];
    for (int i=0; i<10; i++){
        scanf("%d", &a[i]);
    }
    inverte(a, y, 10);

    for (int i=0; i<10; i++){
        printf("%d ", y[i]);
    }

    return 0;
}

int inverte(int *vx, int *vy, int n){
    for( int i=0; i<10; i++){
        vy[9-i] = vx[i]; 
    }

    return 0;
}