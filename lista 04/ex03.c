#include <stdio.h>

int main()
{
int numbers[5]; // 1. declara um vetor int de 5 posições
int *p; // 2. declara um ponteiro p
int n; // 3. declara um inteiro n

p = numbers; // 4. ponteiro p recebe o endereço do ponteiro de vetor numbers
*p = 10; // 5. o primeiro elemento do vetor numbers recebe 10 {10, , , , }

p++; // 6. incrementa o ponteiro (que inicialmente apontava para o elemento 0 do vetor)
*p = 20; // 7. o elemento 1 do vetor recebe 20 {10, 20, , , }

p = &numbers[2]; // 8. p recebe o endereço do elemento 2 do vetor numbers
*p = 30; // 9. o elemento 2 do vetor nnumbers recebe 30 {10, 20, 30, , }

p = numbers + 3; // 10. p recebe o endereço do ponteiro de vetor +3 
*p = 40; // 11. o elemento 3 do vetor recebe 40 {10, 20, 30, 40, }

p = numbers; // 12. p novamente recebe o endereço do ponteiro de vetor (o fazendo apontar novamente para o primeiro elemento do vetor)
*(p + 4) = 50; // 13. o último elemento do vetor recebe 50 {10, 20, 30, 40, 50}

for (n = 0; n < 5; n++)
printf("%d ", numbers[n]); // imprime cada elemento do vetor numbers (10, 20, 30, 40, 50)

return 0;
}