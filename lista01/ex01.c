#include <stdio.h>
#ifndef SEGUNDOS_HORAS_C_INCLUDED
#define SEGUNDOS_HORAS_C_INCLUDED
#endif // SEGUNDOS_HORAS_C_INCLUDED


int main(){
    int horas, minutos, segundos;

    printf("digite o tempo em segundos: ");
    scanf("%d", &segundos);

    horas = segundos / 3600;
    minutos = (segundos%3600) / 60;
    segundos = (segundos%3600)%60;

    printf("horas: %d minutos: %d segundos: %d", horas, minutos, segundos);

    return 0;
}
