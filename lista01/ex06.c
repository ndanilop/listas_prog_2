#include <stdio.h>
#include <math.h>
int raiz_quadrada(int a, int b , int c, float *p1, float*p2);

int main()
{
    int a, b, c;
    float raiz_a, raiz_b;
//
    printf("digite os termos de uma equação do segundo grau; \n");
    printf("a: ");
    scanf("%d", &a);
    printf("b: ");
    scanf("%d", &b);
    printf("c: ");
    scanf("%d", &c);
    raiz_quadrada(a, b, c, &raiz_a, &raiz_b);
    printf("raizes = %.0f, %.0f", raiz_a, raiz_b);
    

    return 0;
}
int raiz_quadrada(int a, int b, int c, float *p1, float *p2){
    int delta;
    delta = (b*b) -4*a*c;
    //printf("%d \n", delta);
    *p1 = ((-1*b)+sqrt(delta))/(2*a);
    *p2 = ((-1*b)-sqrt(delta))/(2*a);
}