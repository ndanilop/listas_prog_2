#include <stdio.h>

int main()
{
    int a, b, c, d, x;
    float e, f, g, h;
    char v[10];
    int *p = &a;

    printf("Endereco de a: %p\n", p);
    printf("Valor em a: %d\n", *p);

    // Verificar se o endereço coincide com o endereço de alguma outra variável
    if(p == &b || p == &c || p == &d ) {
        printf("O endereco coincide com o endereço de alguma outra variável.\n");
    } else {
        printf("O endereco não coincide com o endereço de alguma outra variável.\n");
    }

    return 0;
}
