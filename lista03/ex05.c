#include <stdio.h>

float max_vet (int n, float * vet);

int main(){

    float vetor[5] = {90.1, 2.2, 157.3, 4.4, 5.5};
    printf("maior valor: %.1f", max_vet(5, vetor));

    return 0;
}

float max_vet (int n, float * vet){
    int i;
    float max = vet[0];
    for(i=0; i < n; i++){
        if(vet[i]>= max){
            max = vet[i];
        }
    }
    return max;
}