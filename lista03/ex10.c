#include <stdio.h>

float media(int n, float *v);

int main()
{
    float v[5] = {7.5, 7.5, 7.5, 7.5, 8.5};

    printf("a media do vetor 5 é: %.1f", media(5, v));


    return 0;
}

float media(int n, float *v){
    float soma = 0;
    for (int i = 0; i < n; i++)
    {
        soma+= v[i];
    }

    return soma/n;
    
}