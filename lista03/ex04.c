#include <stdio.h>

int divs (int n, int *max, int *min);

int main()
{
    int a, b, c=1;
    scanf("%d", &a);
    int result = divs(a, &b, &c);
    if(result == 0){
        printf("resultado: %d", result);
    }else{
        printf("resultado: %d  max: %d  min: %d", result, b, c);
    }
    return 0;
}

int divs (int n, int *max, int *min){
    int i, cont=0;
    for(i=2; i<= (n/2); i++){
        if (n%i == 0){
            *min = i;
            cont++;
            break;
        }
        
    }

    if(cont==0){
        return 0;
    }else{
        for(i=2; i<=(n/2); i++){
            if (n%i == 0){*max = i;}
            }
        return 1;
    }
}