#include <stdio.h>
#include <math.h>
void calc_circulo(float r, float * circunferencia, float * area);

int main()
{
    float raio, circ, area;
    scanf("%f", &raio);
    calc_circulo(raio, &circ, &area);
    printf("area : %.2f circunferencia: %.2f", area, circ);
    return 0;
}

void calc_circulo(float r, float * circunferencia, float * area){
    *circunferencia = 3.14159265*(pow(r, 2));
    *area = 2.0*3.14159265*r;

}
